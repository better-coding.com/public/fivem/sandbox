﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MysqlChatStoreClient
{
    public class ChatStoreClient : BaseScript
    {
        public ChatStoreClient() {
            EventHandlers["onClientResourceStart"] += new Action<string>(OnClientResourceStart);
        }

        private void OnClientResourceStart(string resourceName) {
            if (API.GetCurrentResourceName() != resourceName) {
                return;
            }

            API.RegisterCommand("save", new Action<int, List<object>, string>((source, args, raw) => {
                string message = String.Join(" ", args);
                TriggerEvent("chat:addMessage", new { 
                    args = new[] { "[ChatStoreClient]:", $"Said: {message}" }
                });

                TriggerServerEvent("bc:storeMessage", message);
            } ), false);

            Debug.WriteLine($"ChatStoreClient: >> Resource {resourceName} loaded!");
        }
    }
}
