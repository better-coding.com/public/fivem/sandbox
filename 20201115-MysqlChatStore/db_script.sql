drop table if exists message;

create table message( 
id int auto_increment primary key,
player_name varchar(200),
message_text varchar(2000)
);

insert into message (player_name, message_text) values ("Unknown 1", "Message 1");
insert into message (player_name, message_text) values ("Unknown 2", "Message 2");

select * from message;

SELECT count(0) from message



-- TO create my schema you can use the following scripts

CREATE DATABASE sandbox01
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

use sandbox01;
