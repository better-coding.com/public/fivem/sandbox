﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MysqlChatStoreServer
{
    public class ChatStoreServer : BaseScript
    {
        public ChatStoreServer() {
            EventHandlers["onResourceStart"] += new Action<string>(OnResourceStart);
            EventHandlers["bc:storeMessage"] += new Action<Player, string>(StoreMessage);
        }

        private void StoreMessage([FromSource] Player player, string message) {
            Debug.WriteLine($"ChatStoreServer: >> Got message '{message}' from player {player.Name}");
            storeChatMessageIntoDB(player.Name, message);
        }

        private async Task storeChatMessageIntoDB(string playerName, string message) {
            string connStr = "server=localhost;user=sandbox01;database=sandbox01;port=3306;password=welcome1";
            Debug.WriteLine($"ChatStoreServer: >> Connecting to the database");

            MySqlConnection conn = new MySqlConnection(connStr);

            try {
                await conn.OpenAsync();

                string insertStmt = "insert into message (player_name, message_text) values (@player_name, @message)";
                MySqlCommand command = new MySqlCommand(insertStmt, conn);
                command.Parameters.AddWithValue("@player_name", playerName);
                command.Parameters.AddWithValue("@message", message);

                long rowsNo = (long)await command.ExecuteNonQueryAsync();

                Debug.WriteLine($"ChatStoreServer: >> Rows inserted: {rowsNo} ");

            } catch (Exception ex) {
                Debug.WriteLine($"ChatStoreServer: >> {ex} ");
            }

            conn.Close();

            Debug.WriteLine($"ChatStoreServer: >> Disconnected");

            displayTotalNumberOfMessages();
        }

        private void OnResourceStart(string resourceName) {
            if (API.GetCurrentResourceName() != resourceName) {
                return;
            }

            displayTotalNumberOfMessages();

            Debug.WriteLine($"ChatStoreServer: >> Resource {resourceName} loaded!");
        }

        private async Task displayTotalNumberOfMessages() {
            string connStr = "server=localhost;user=sandbox01;database=sandbox01;port=3306;password=welcome1";
            Debug.WriteLine($"ChatStoreServer: >> Connecting to the database");

            MySqlConnection conn = new MySqlConnection(connStr);

            try {
                await conn.OpenAsync();

                string testSQL = "SELECT count(0) from message";
                MySqlCommand command = new MySqlCommand(testSQL, conn);
                 
                long messagesNo = (long)await command.ExecuteScalarAsync();

                Debug.WriteLine($"ChatStoreServer: >> Total messages stored in database: {messagesNo} ");

            } catch (Exception ex) {
                Debug.WriteLine($"ChatStoreServer: >> {ex} ");
            }

            conn.Close();

            Debug.WriteLine($"ChatStoreServer: >> Disconnected");
        }
    }
}
